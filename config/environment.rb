# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
ApproximationCalculator::Application.initialize!
ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.raise_delivery_errors = true
ActionMailer::Base.smtp_settings = {
  :address        => "smtp.gmail.com",
  :port           => 587,
  :domain         => "gmail.com",
  :user_name      => "approximationcalculator@gmail.com",
  :password       => "cs410umb",
  :authentication => :login
}