class AddTeacherField < ActiveRecord::Migration
  def change
    add_column :users, :teacher, :string, :default => ""
  end
end
