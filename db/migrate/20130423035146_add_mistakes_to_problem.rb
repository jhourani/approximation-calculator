class AddMistakesToProblem < ActiveRecord::Migration
  def change
    add_column :problems, :mistakes, :integer
  end
end
