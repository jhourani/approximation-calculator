class CreateProblems < ActiveRecord::Migration
  def change
    create_table :problems do |t|
      t.integer :a
      t.integer :b
      t.integer :x
      t.integer :y
      t.integer :answer
      t.string :sign
      t.integer :time
      t.string :user

      t.timestamps
    end
  end
end
