class FeedbackMailer < ActionMailer::Base
  default from: "from@example.com"

  def feedback(feedback)
    recipients  = 'jordanhourani@gmail.com'
    subject     = "[Feedback for Approximation Calculator] #{feedback.subject}"

    @feedback = feedback
    mail(:to => recipients, :subject => subject)
  end
end
