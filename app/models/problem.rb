class Problem < ActiveRecord::Base
  belongs_to :user
  attr_accessible :a, :answer, :b, :sign, :time, :user, :x, :y, :mistakes, :user_id
end
