class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  has_many :problems
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable


  
  # in User.rb
  attr_accessible :email, :password, :password_confirmation, :remember_me,
				:username, :role, :teacher, :admin
  # attr_accessible :title, :body

end
