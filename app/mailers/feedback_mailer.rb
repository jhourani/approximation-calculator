class FeedbackMailer < ActionMailer::Base
  default from: "approximationcalculator@gmail.com"

  def feedback(feedback)
    recipients  = 'jordanhourani@gmail.com', 'daniel.housley001@umb.edu', 'evan.simons@gmail.com', 'neilb228@gmail.com'
    subject     = "[You have received feedback from a user.] #{feedback.subject}"

    @feedback = feedback
    mail(:to => recipients, :subject => subject)
  end
end
