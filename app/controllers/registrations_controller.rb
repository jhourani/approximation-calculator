class RegistrationsController < Devise::RegistrationsController
  before_filter :authenticate_user!
  
  def create
  	super
  end
  
  private
	def redirect_unless_admin
  		unless current_user.role? == "admin" || "teacher"       
    		redirect_to root_path
	end
  end
end
