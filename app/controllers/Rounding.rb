#Rounding.rb

class Rounding

	#Called when a Rounding object is instantiated
	#Values i and j should be integers; these are the original, unrounded numbers
	def initialize(i, j, sign, allow, threshold)
		@a = i.to_i
		@b = j.to_i
		@a_poss = []
		@b_poss = []
		@allow = allow
		@threshold = threshold
		@sign = sign
		if sign == '+' || sign == '-'
			possAddSubLists(i, j)
		elsif sign == '*'
			possMultiLists(i, j)
		elsif sign == '/'
			possDivLists(i, j)
		end
	end
	
	def possAddSubLists(i, j)
		signa = 1
		signa = -1 if @a < 0
		a = (signa*@a).to_s
		signb = 1
		signb = -1 if @b < 0
		b = (signb*@b).to_s
		if a.length > b.length
			j = a.length - b.length
			b = '0'*j + b
		else
			j = b.length - a.length
			a = '0'*j + a
		end
		signa = ""		
		signa = "-" if @a < 0
		signb = ""		
		signb = "-" if @b < 0
		defPossLists(signa+a, signb+b)
	end
	
	def possMultiLists(i, j)
		defPossLists(i.to_s, j.to_s)
	end
	
	def possDivLists(i, j)
		defPossLists2(@b_poss, j.to_s)
		@b_poss.insert(2, i, j)
		@b_poss.delete(nil)		
		@b_poss.delete(0)
		@b_poss.delete(false)
		@b_poss.uniq!
		@a_poss = Hash.new([])
		@b_poss.each {|b| 
			div = i.to_i/b.to_i
			pn = 1
			array = Array.new
			array << b.to_i
			array << i.to_i if i%b==0
			array << roundTo5(div, 1)*b if @allow[:to5] && roundTo5(div, 1)
			if @allow[:to10]
				m = roundUp10(div, 1)
				array << (m*b) if m!=false
				n = roundDown10(div, 1)
				array << (n*b) if n!=false
				pn = -1 if i<0
				o = roundDown10(i.abs, 1)
				array << (pn*o) if o!=false && o%b==0
				p = roundUp10(i.abs, 1)
				array << (pn*p) if p!=false && p%b==0
			end
			if @allow[:to15]
				m = roundUp15(div, 1)
				array << (m*b) if m!=false
				n = roundDown15(div, 1)
				array << (n*b) if m!=false
			end
			if @allow[:to25]
				m = roundUp25(div, 1)
				array << (m*b) if m!=false
				n = roundDown25(div, 1)
				array << (n*b) if m!=false
			end
			array.delete(nil)
			array.delete(false)
			array.uniq!
			@a_poss[b] = array
		}
	end
	
	def defPossLists(i, j)
		defPossLists2(@a_poss, i)
		defPossLists2(@b_poss, j)
	end
	
	def defPossLists2(array, i)
		pn = 1
		if (i[0]=="-")
			pn = -1
			i=i.slice(1,i.length-1)
		end
		array2 = []
		array2 << roundTo5(i, 1) if @allow[:to5]
		if i.length > 2
			array2.insert(2, roundUp10(i, 2), roundDown10(i, 2)) if @allow[:to10]
			array2.insert(2, roundUp15(i, 2), roundDown15(i, 2)) if @allow[:to15]
			array2.insert(2, roundUp25(i, 2), roundDown25(i, 2)) if @allow[:to25]
		end
		array2.insert(2, roundUp10(i, 1), roundDown10(i, 1)) if @allow[:to10]
		array2.insert(2, roundUp15(i, 1), roundDown15(i, 1)) if @allow[:to15]
		array2.insert(2, roundUp25(i, 1), roundDown25(i, 1)) if @allow[:to25]		
		array2 << i if i.to_i < 10 #dealing with the case of 1-digit numbers, relevant for multiplication/division
		array2.delete(nil)
		array2.delete(false)
		array2.uniq!
		array2.each {|x| array << pn*x.to_i}
	end
	
	#Returns whether numbers given constitute good rounded values of initialized integers
	def goodRound?(x, y)
		if @sign=='+' || @sign=='-' then return (@a_poss.include?(x) and @b_poss.include?(y))
		elsif @sign=='*'
			return (@a_poss.include?(x) and @b_poss.include?(y)) || (@a=x and y.to_s =~ /^10+$/) || (@b=y and x.to_s =~ /^10+$/)
		elsif @sign =='/' then return (@a_poss[y].include?(x))
		end
	end
	
	#This method is a test to see if integers x and y are considered good roundings of the original values i and j
	#Given integers x and y, returns a boolean
	def tightAddEst?(x, y)		
		return (((@a+@b)-(x+y)).abs.to_f/(@a+@b)<=@threshold)
	end
	
	def tightSubEst?(x, y)
		return (((@a-@b)-(x-y)).abs.to_f/(@a-@b)<=@threshold || ((@a-@b)-(x-y)).abs<=5)
	end

	def tightMultiEst? (x, y)
		return (((@a*@b)-(x*y)).abs.to_f/(@a*@b)<=@threshold)
	end

	def tightDivEst? (x, y)
		adj_thresh = @threshold + 0.03
		ceil = (@a.to_f/@b).ceil
		flr = (@a.to_f/@b).floor
		return ((flr-(x.to_f/y)).abs/flr<=adj_thresh || (ceil-(x.to_f/y)).abs/ceil<=adj_thresh)
	end

	#This method is just a quick way to test program behavior
	def demoSum
		puts "\nFor numbers A:" + @a.to_s + " and B:" + @b.to_s + ", the accepted roundings would be:"
		print "A:"
		@a_poss.each {|x| print " {" + x.to_s + "}"}
		print "\nB:"
		@b_poss.each {|x| print " {" + x.to_s + "}"}
		print "\n"
	end
	
	def demoSum2
		puts "\nFor numbers " + @a.to_s + " and " + @b.to_s + ", the accepted roundings would be:"
		@a_poss.each {|x| @b_poss.each {|y| puts x.to_s + ", " + y.to_s + "  <-- margin of accuracy: " + (1-((@a.to_i+@b.to_i)-(x+y)).abs.to_f/(@a.to_i+@b.to_i)).to_s if goodRound?(x, y)}}		
	end	

	#This method rounds to 5
	def roundTo5(x, index)
		return 5 if x.to_i<10
		s = x.to_s
		if (index == 0) then return ('5' + '0'*(s.length-1)).to_i
		elsif index > 0 then return (s[0..index-1] + '5' + '0'*(s.length-1-index)).to_i
		else return false
		end
	end
	
	#This method rounds UP to the nearest multiple of 10
	def roundUp10(x, index)
		return 10 if x.to_i<10
		s = x.to_s
		return false if index+1 > s.length || index < 0
		if (index == 0)
			return ('1'+'0'*(s.length)).to_i
		else
			s = '0'+s
			k = s.length-1
			while (k>index or s[k]=='9')
				s[k]='0'
				k -= 1
			end
			s[k] = (s[k].to_i + 1).to_s
			return s.to_i
		end
	end
	
	#This method rounds DOWN to the nearest multiple of 10
	def roundDown10(x, index)
		return 0 if x.to_i<10
		s = x.to_s
		return false if index+1 > s.length || index < 0
		if (index == 0)
			t = '0' + s[0]
		else
			t = s[index-1..index]
		end
		t = (t.to_f/10).floor*10
		t = '00' if t == 0
		if (index < 2)
			return (t.to_s + "0"*(s.length-2)).to_i
		else 
			return (s[0..index-2] + t.to_s + "0"*(s.length-1-index)).to_i
		end
	end
	
	#This method rounds UP to the nearest multiple of 15
	def roundUp15(x, index)
		s = x.to_s
		return false if index+1 > s.length || index < 0
		if (index == 0)
			t = '0' + s[0]
		else
			t = s[index-1..index]
		end
		t = (t.to_f/15).ceil*15
		if (t>30) #Subject to change if client wants to be able to round to anything besides 15 and 30
			return false
		end
		if (index < 2)
			return (t.to_s + "0"*(s.length-2)).to_i
		else 
			return (s[0..index-2] + t.to_s + "0"*(s.length-1-index)).to_i
		end
	end
	
	#This method rounds DOWN to the nearest multiple of 15
	def roundDown15(x, index)
		s = x.to_s
		return false if index+1 > s.length || index < 0
		if (index == 0)
			t = '0' + s[0]
		else
			t = s[index-1..index]
		end
		t = (t.to_f/15).floor*15
		if (t>30) #Subject to change if client wants to be able to round to anything besides 15 and 30
			return
		end
		t = '00' if t == 0
		if (index < 2)
			return (t.to_s + "0"*(s.length-2)).to_i
		else 
			return (s[0..index-2] + t.to_s + "0"*(s.length-1-index)).to_i
		end
	end
	
	#This method rounds UP to the nearest multiple of 25
	def roundUp25(x, index)
		s = x.to_s
		return false if index+1 > s.length || index < 0
		if (index == 0)
			t = '0' + s[0]
		else
			t = s[index-1..index]
		end
		t = (t.to_f/25).ceil*25
		if (index < 2)
			return (t.to_s + "0"*(s.length-2)).to_i
		else 
			return (s[0..index-2] + t.to_s + "0"*(s.length-1-index)).to_i
		end
	end
	
	#This method rounds DOWN to the nearest multiple of 25
	def roundDown25(x, index)
		s = x.to_s
		return false if index+1 > s.length || index < 0
		if (index == 0)
			t = '0' + s[0]
		else
			t = s[index-1..index]
		end
		t = (t.to_f/25).floor*25
		t = '00' if t == 0
		if (index < 2)
			return (t.to_s + "0"*(s.length-2)).to_i
		else 
			return (s[0..index-2] + t.to_s + "0"*(s.length-1-index)).to_i
		end
	end
end
=begin
puts "Enter an integer: "
i = gets
puts "\nEnter another integer: "
j = gets
print "\nEnter the sign: "
sign = gets
allow = { :to5 => true, :to10 => true, :to15 => true, :to25 => true }
k = Rounding.new(i.chomp.to_i, j.chomp.to_i, sign.chomp, allow, 0.15)
k.demoSum
=end