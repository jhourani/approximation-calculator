class HomeController < ApplicationController
	before_filter :authenticate_user!
	def index
	end
	@message = "No message"
	#This method is a test to see if integers x and y are considered good roundings of the original values i and j
	#Given integers x and y, returns a boolean
	def goodAddRound
		@amodel = params[:a].to_i
		@bmodel = params[:b].to_i
		@xmodel = params[:x].to_i
		@ymodel = params[:y].to_i
		a_poss = [@amodel, round(@amodel), round2(@amodel)].uniq
		b_poss = [@bmodel, round(@bmodel), round2(@bmodel)].uniq	
		if (a_poss.include?(@xmodel) and b_poss.include?(@ymodel) and ((@amodel+@bmodel)-(@xmodel+@ymodel)).abs.to_f/(@amodel+@bmodel)<0.03)
			true
			@message = "Good job rounding!"
		else
			false
			@message = "Bad job rounding!"
		end
	end
	
# goodSubRound? (int orig1, int orig2, int round1, int round2)

# goodMultiRound? (int orig1, int orig2, int round1, int round2)

# goodDivRound? (int orig1, int orig2, int round1, int round2)

	#This method rounds to the nearest multiple of 5 in the second-largest place
	def round(i)
		return i if i<10
		i0 = (i.to_s)[0]
		i1 = (i.to_s)[1]
		if i1.to_i < 3
			i1 = "0"
		elsif i1.to_i < 8
			i1 = "5"
		elsif i1.to_i < 10
			i1 = "0"
			i0 = (i0.to_i + 1).to_s
		end
		returnval = i0 + i1 + ("0" * (i.to_s.length-2))
		returnval.to_i
	end
	
	#This method rounds to the nearest multiple of 5 in the third-largest place
	def round2(i)
		return round(i) if i<100
		i0 = (i.to_s)[0]
		i1 = (i.to_s)[1]
		i2 = (i.to_s)[2]
		if i2.to_i < 3
			i2 = "0"
		elsif i2.to_i < 8
			i2 = "5"
		elsif i2.to_i < 10
			i2 = "0"
			if i1 == "9"
				i1 = "0"
				i0 = (i0.to_i + 1).to_s
			else i1 = (i1.to_i + 1).to_s
			end
		end
		returnval = i0 + i1 + i2 + ("0" * (i.to_s.length-3))
		returnval.to_i
	end
end
