class ProblemsController < ApplicationController
	before_filter :authenticate_user!	
	require("Rounding.rb")
	require("csv")
	
  # GET /problems
  # GET /problems.json
  def index
    @problems = Problem.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @problems }
    end
  end

  # GET /problems/1
  # GET /problems/1.json
  def show
    @problem = Problem.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @problem }
    end
  end

  # GET /problems/new
  # GET /problems/new.json
  def new
    @problem = Problem.new
	@problem.sign = '+'

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @problem }
    end
  end

  # GET /problems/1/edit
  def edit
    @problem = Problem.find(params[:id])
  end
  
  # GET /problems/1/retry
  def retry
	@problem = Problem.find(params[:id])
  end
  
  # GET /problems/data
  def data
	@problems = Problem.all
	@users = User.all		
	incorrect = []
	correct = []
	@users.each do |u|
		totalproblems = u.problems
		correctproblems = totalproblems.where(:mistakes=>0)
		incorrectproblems = totalproblems - correctproblems
		incorrect<<[u.username, incorrectproblems.count]
		correct<<[u.username, correctproblems.count]
	end
	@graph = LazyHighCharts::HighChart.new('column') do |f|
		f.series(:name=>'Incorrect',:data=>incorrect,:color=>'#0d233a')
		f.series(:name=>'Correct',:data=>correct,:color=>'#2f7ed8')
	f.xAxis(:type=>"category", :labels=>{:staggerLines=>2})
    f.title(:text=>"Problems per student")
	f.chart(:zoomType=>"x", :resetZoomButton=>{:position=>{:y=>-25}})

    ###  Options for Bar
    ### f.options[:chart][:defaultSeriesType] = "bar"
    ### f.plot_options({:series=>{:stacking=>"normal"}}) 

    ## or options for column
    f.options[:chart][:defaultSeriesType] = "column"
    f.plot_options(:column=>{:stacking=>"normal"})
	end
  end
  
  #GET /problems/exportCSV
  def exportCSV
	csv_string = CSV.generate do |csv|
		csv << ["User Name", "Original A", "Original B", "Sign", "Rounded X", "Rounded Y", "Final Answer", "Correct?", "Time Taken", "Time Stamp"]
		Problem.all.each do |problem|
			username = User.find(problem.user_id).username
			csv << [username, problem.a, problem.b, problem.sign, problem.x, problem.y, problem.answer, problem.mistakes==0, problem.time, problem.created_at]
		end
	end
	send_data(csv_string, :filename=>'StudentData_' + Date.today.to_s + '.csv', :type=>'text/csv', :disposition=>'attachment')
  end

  # POST /problems
  # POST /problems.json
  def create  
    @problem = current_user.problems.create(params[:problem])
	allow = { :to5 => true, :to10 => true, :to15 => true, :to25 => true } #default
	rounding = Rounding.new(@problem.a, @problem.b, @problem.sign, allow, 0.15)

    respond_to do |format|
		@problem.mistakes = 0
		if @problem.sign == '+'
			tight = (!rounding.tightAddEst?(@problem.x, @problem.y))
			realAnswer = @problem.a + @problem.b
			incorrect = (@problem.x + @problem.y != @problem.answer)
		elsif @problem.sign == '-'
			tight = (!rounding.tightSubEst?(@problem.x, @problem.y))
			realAnswer = @problem.a - @problem.b
			incorrect = (@problem.x - @problem.y != @problem.answer)
		elsif @problem.sign == '*'
			tight = (!rounding.tightMultiEst?(@problem.x, @problem.y))
			realAnswer = @problem.a * @problem.b
			incorrect = (@problem.x * @problem.y != @problem.answer)
		elsif @problem.sign == '/'
			tight = (!rounding.tightDivEst?(@problem.x, @problem.y))	
			realAnswer = (@problem.a.to_f / @problem.b).round(2)	
			incorrect = (@problem.x / @problem.y != @problem.answer)
		end
		@mistakes = [incorrect, (!rounding.goodRound?(@problem.x, @problem.y)), tight]
		if @mistakes.include?(true)			
			aOrig = @problem.a #Copy the problem's features into temp variables -- so the form doesn't reset
			bOrig = @problem.b
			xOrig = @problem.x
			yOrig = @problem.y
			signOrig = @problem.sign
			@problem.mistakes += 100 if @mistakes[0]
			@problem.mistakes += 20 if @mistakes[1]
			@problem.mistakes += 3 if @mistakes[2]
			if @problem.save
				@problem = Problem.new(params[:problem])
				@problem.a = aOrig
				@problem.b = bOrig
				@problem.x = xOrig
				@problem.y = yOrig	
				@problem.sign = signOrig
				@problem.errors.add(:Tightness, ": Your estimate isn't close enough to the actual. Try a different rounding in Step 2.") if @mistakes[2]
				@problem.errors.add(:Rounding, ": There's a mistake in your rounded problem set. Check your work in Step 2.") if @mistakes[1]
				@problem.errors.add(:Incorrect, ": The answer you gave isn't correct for the rounded problem set you provided.") if @mistakes[0]
				format.html { render action: "new" }
			else
				format.html { render action: "new" }
				format.json { render json: @problem.errors, status: :unprocessable_entity }
			end
		else
			if @problem.save
				@problem = Problem.new(params[:problem])
				@success = ("Good job! The actual answer was " + realAnswer.to_s + ".")
				format.html { render action: "new" }
				#flash[:notice] = ("Good job! The actual answer is " + realAnswer.to_s + ".")
				#format.html { redirect_to :action=>"new" }
				#format.html { redirect_to @problem, notice: 'Problem was successfully created.' }
				#format.json { render json: @problem, status: :created, location: @problem }
			else
				format.html { render action: "new" }
				format.json { render json: @problem.errors, status: :unprocessable_entity }
			end
		end
    end
  end

  # PUT /problems/1
  # PUT /problems/1.json
  def update
    @problem = Problem.find(params[:id])

    respond_to do |format|
      if @problem.update_attributes(params[:problem])
        format.html { redirect_to @problem, notice: 'Problem was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @problem.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /problems/1
  # DELETE /problems/1.json
  def destroy
    @problem = Problem.find(params[:id])
    @problem.destroy

    respond_to do |format|
      format.html { redirect_to problems_url }
      format.json { head :no_content }
    end
  end
end
